package by.anegin.circle_maze;

import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.wearable.activity.WearableActivity;

import by.anegin.circle_maze.common.dyn.CircleMazeWorld;
import by.anegin.circle_maze.common.utils.SensorHelper;
import by.anegin.circle_maze.common.view.CircleMazeView;

public class MainActivity extends WearableActivity implements SensorHelper.Listener, CircleMazeWorld.Listener {

	private static final double C = (5d / 7d) * SensorManager.GRAVITY_EARTH;

	// - на часах включить Отладку по Bluetooth
	// - на телефоне в Android Wear в настройках часов включить Отладку по Bluetooth
	// - на компе подключить adb к часам:
	//     adb forward tcp:4444 localabstract:/adb-hub
	//     adb connect 127.0.0.1:4444

	private CircleMazeView circleMazeView;

	private SensorHelper sensorHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(by.anegin.circle_maze.R.layout.activity_main);
		setAmbientEnabled();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		int points = prefs.getInt(Constants.PREF_SCORE, 0);

		circleMazeView = findViewById(R.id.circle_maze_view);
		circleMazeView.setCircleMazeWorldListener(this);
		circleMazeView.setScore(points);

		sensorHelper = new SensorHelper(this, 30, new Handler(), this);
	}

	@Override
	public void onResume() {
		super.onResume();
		sensorHelper.resume();
	}

	@Override
	protected void onPause() {
		sensorHelper.pause();
		super.onPause();
	}

	@Override
	public void onEnterAmbient(Bundle ambientDetails) {
		super.onEnterAmbient(ambientDetails);
		boolean lowBit = ambientDetails.getBoolean(EXTRA_LOWBIT_AMBIENT, false);
		boolean burnInProtection = ambientDetails.getBoolean(EXTRA_BURN_IN_PROTECTION, false);
		circleMazeView.setDrawMode(true, lowBit || burnInProtection, false);
	}

	@Override
	public void onExitAmbient() {
		super.onExitAmbient();
		circleMazeView.setDrawMode(false, false, false);
	}

	@Override
	public void onDeviceRotate(double azimuth, double pitch, double roll) {
		double coef = 2.0;
		double ax = C * Math.sin(roll) * coef;
		double ay = C * Math.sin(pitch) * coef;
		circleMazeView.setGravity(ax, ay);
	}

	@Override
	public void onBallReachedCenter() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		int points = prefs.getInt(Constants.PREF_SCORE, 0) + 1;
		prefs.edit().putInt(Constants.PREF_SCORE, points).apply();

		runOnUiThread(() -> {
			circleMazeView.setScore(points);
			circleMazeView.regenerateMaze();
		});
	}

}