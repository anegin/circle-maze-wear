package by.anegin.circle_maze.watch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceService;
import android.util.TypedValue;
import android.view.SurfaceHolder;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import androidx.core.content.ContextCompat;
import by.anegin.circle_maze.Constants;
import by.anegin.circle_maze.R;
import by.anegin.circle_maze.common.dyn.CircleMazeWorld;
import by.anegin.circle_maze.common.maze.CircleMaze;
import by.anegin.circle_maze.common.model.Clock;
import by.anegin.circle_maze.common.model.MazeSwitcher;
import by.anegin.circle_maze.common.utils.SensorHelper;

public class CircleMazeWatchFace extends CanvasWatchFaceService {

	private static final int MAZE_CYLINDERS = 4;
	private static final int MAZE_INITIAL_SECTORS = 8;

	private final CircleMaze maze;

	public CircleMazeWatchFace() {
		maze = new CircleMaze(MAZE_CYLINDERS, MAZE_INITIAL_SECTORS);
		maze.generate();
	}

	@Override
	public Engine onCreateEngine() {
		return new CircleMazeWatchFaceEngine(maze);
	}

	private class CircleMazeWatchFaceEngine extends CanvasWatchFaceService.Engine implements SensorHelper.Listener, CircleMazeWorld.Listener {

		private static final double C = (5d / 7d) * SensorManager.GRAVITY_EARTH;

		private static final double MAX_WORLD_SIZE = 10.0;  // размер поля в метрах

		private Calendar mCalendar;

		private CircleMaze maze;
		private CircleMazeWorld world;
		private CircleMazeUpdateThread updateThread;

		private MazeSwitcher mazeSwitcher;

		private Clock clock;

		private SensorHelper sensorHelper;

		private boolean mAmbient;
		private boolean mLowBitAmbient;
		private boolean mBurnInProtection;
		private boolean mMuteMode;

		private boolean isTimeZoneReceiverRegistered;

		private Handler uiHandler;

		CircleMazeWatchFaceEngine(CircleMaze maze) {
			this.maze = maze;
			uiHandler = new Handler();
		}

		@Override
		public void onCreate(SurfaceHolder holder) {
			super.onCreate(holder);

			int bgColor = ContextCompat.getColor(getApplicationContext(), R.color.maze_bg);
			int wallColor = ContextCompat.getColor(getApplicationContext(), R.color.maze_walls);
			int ballColor = ContextCompat.getColor(getApplicationContext(), R.color.maze_ball);

			float dp32 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getApplicationContext().getResources().getDisplayMetrics());
			world = new CircleMazeWorld(null, bgColor, wallColor, ballColor, MAX_WORLD_SIZE);
			world.setDrawPadding(dp32);
			world.setTestMode(false);
			world.setMaze(maze);
			world.setListener(this);

			mazeSwitcher = new MazeSwitcher(wallColor);

			mCalendar = GregorianCalendar.getInstance(Locale.getDefault());
			clock = new Clock(dp32, mCalendar, wallColor);

			updateThread = new CircleMazeUpdateThread(world, mazeSwitcher, clock, holder);
			updateThread.start();

			sensorHelper = new SensorHelper(getApplicationContext(), 30, new Handler(), this);
		}

		@Override
		public void onDestroy() {
			sensorHelper.pause();

			if (updateThread != null) {
				boolean retry = true;
				updateThread.finish();
				while (retry) {
					try {
						updateThread.join();
						retry = false;
					} catch (InterruptedException ignored) {
					}
				}
				updateThread = null;
			}

			super.onDestroy();
		}

		@Override
		public void onPropertiesChanged(Bundle properties) {
			super.onPropertiesChanged(properties);
			mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
			mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);
			updateDrawMode();
		}

		@Override
		public void onTimeTick() {
			super.onTimeTick();
			invalidate();
		}

		@Override
		public void onAmbientModeChanged(boolean inAmbientMode) {
			super.onAmbientModeChanged(inAmbientMode);
			mAmbient = inAmbientMode;
			if (inAmbientMode) {
				sensorHelper.pause();
			} else {
				sensorHelper.resume();
			}
			updateThread.setPaused(inAmbientMode);
			updateDrawMode();
		}

		private void updateDrawMode() {
			world.setDrawMode(mAmbient, mLowBitAmbient || mBurnInProtection, mMuteMode);
			clock.setDrawMode(mAmbient, mLowBitAmbient || mBurnInProtection, mMuteMode);
			invalidate();
		}

		@Override
		public void onInterruptionFilterChanged(int interruptionFilter) {
			super.onInterruptionFilterChanged(interruptionFilter);
			boolean inMuteMode = (interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE);
			if (mMuteMode != inMuteMode) {
				mMuteMode = inMuteMode;
				updateDrawMode();
			}
		}

		@Override
		public void onDraw(Canvas canvas, Rect bounds) {
			updateThread.invalidate(bounds);
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			super.onVisibilityChanged(visible);
			mCalendar.setTimeZone(TimeZone.getDefault());
			if (visible) {
				if (!isTimeZoneReceiverRegistered) {
					isTimeZoneReceiverRegistered = true;
					registerReceiver(mTimeZoneReceiver, new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED));
				}
				sensorHelper.resume();
			} else {
				if (isTimeZoneReceiverRegistered) {
					isTimeZoneReceiverRegistered = false;
					unregisterReceiver(mTimeZoneReceiver);
				}
				sensorHelper.pause();
			}
			updateThread.setPaused(!visible);
		}

		private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				mCalendar.setTimeZone(TimeZone.getDefault());
				invalidate();
			}
		};

		@Override
		public void onDeviceRotate(double azimuth, double pitch, double roll) {
			double coef = 2.0;
			double ax = C * Math.sin(roll) * coef;
			double ay = C * Math.sin(pitch) * coef;
			world.setGravity(ax, ay);
		}

		@Override
		public void onBallReachedCenter() {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			int points = prefs.getInt(Constants.PREF_SCORE, 0) + 1;
			prefs.edit().putInt(Constants.PREF_SCORE, points).apply();

			// вызывается не в ui-потоке
			uiHandler.post(() -> {
				mazeSwitcher.setListener(() -> {
					mazeSwitcher.setListener(null);
					CircleMaze maze = new CircleMaze(MAZE_CYLINDERS, MAZE_INITIAL_SECTORS);
					maze.generate();
					world.setMaze(maze);
				});
				mazeSwitcher.animateSwicth();
			});
		}
	}

	private static class CircleMazeUpdateThread extends Thread {

		private final CircleMazeWorld world;
		private final Clock clock;
		private final SurfaceHolder holder;

		private final MazeSwitcher mazeSwitcher;

		private boolean running = true;
		private boolean paused = false;

		private long lastUpdateTime = System.nanoTime();

		private final Object pauseObj = new Object();

		private Rect drawBounds;

		private volatile boolean needInvalidate;

		CircleMazeUpdateThread(CircleMazeWorld world, MazeSwitcher mazeSwitcher, Clock clock, SurfaceHolder holder) {
			this.world = world;
			this.mazeSwitcher = mazeSwitcher;
			this.clock = clock;
			this.holder = holder;
		}

		@Override
		public void run() {
			while (isRunning()) {

				boolean isPaused = isPaused();

				if (!isPaused) {
					if (world.getBodyCount() > 0) {
						long now = System.nanoTime();
						double elapsedTime = (now - lastUpdateTime) / 1.0e9;
						world.update(elapsedTime);
						lastUpdateTime = now;
					}
				}

				if (!isPaused || needInvalidate) {
					needInvalidate = false;
					Canvas canvas = null;
					try {
						canvas = holder.lockCanvas();
						synchronized (holder) {
							if (canvas != null) canvas.drawColor(Color.BLACK);
							world.draw(canvas, drawBounds, 0, 0, 0, null);
							mazeSwitcher.draw(canvas);
							clock.draw(canvas, drawBounds);
						}
					} finally {
						if (canvas != null) {
							holder.unlockCanvasAndPost(canvas);
						}
					}
				}

				try {
					if (isPaused) {
						synchronized (pauseObj) {
							pauseObj.wait(1000);
						}
					} else {
						Thread.sleep(1);
					}
				} catch (InterruptedException ignored) {
				}
			}
		}

		private synchronized void finish() {
			running = false;
		}

		private synchronized boolean isRunning() {
			return running;
		}

		private synchronized void setPaused(boolean paused) {
			this.paused = paused;
			if (!paused) {
				lastUpdateTime = System.nanoTime();
				synchronized (pauseObj) {
					pauseObj.notify();
				}
			}
		}

		private synchronized boolean isPaused() {
			return paused;
		}

		private synchronized void invalidate(Rect bounds) {
			drawBounds = bounds;
			needInvalidate = true;
			if (paused) {
				synchronized (pauseObj) {
					pauseObj.notify();
				}
			}
		}

	}

}