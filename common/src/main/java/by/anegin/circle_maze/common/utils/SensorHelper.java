package by.anegin.circle_maze.common.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

public class SensorHelper implements SensorEventListener {

	private static final float NS2S = 1.0f / 1000000000.0f;
	private static final float EPSILON = 0.000000001f;
	private static final float FILTER_COEFFICIENT = 0.98f;

	private final SensorManager sensorManager;

	private final Sensor sensorAccelerometer;
	private final Sensor sensorGyroscope;
	private final Sensor sensorMagnetic;

	private final float[] accelValues = new float[3];
	private final float[] magnetValues = new float[]{1f, 1f, 1f};
	private final float[] gyroValues = new float[3];

	private final float[] rotationMatrix = new float[9];
	private final float[] accMagOrientation = new float[3];

	private float[] gyroMatrix = new float[]{1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f};   // identity matrix
	private float[] gyroOrientation = new float[]{0f, 0f, 0f};
	private long gyroTimestamp;
	private boolean gyroInitialized;

	private final float[] fusedOrientation = new float[3];

	private final long updatePeriod;
	private Timer updateTimer;

	private final Handler handler;
	private final Listener listener;

	public interface Listener {
		void onDeviceRotate(double azimuth, double pitch, double roll);
	}

	public SensorHelper(Context context, long updatePeriod, Handler handler, Listener listener) {
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorGyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		this.updatePeriod = updatePeriod;
		this.handler = handler;
		this.listener = listener;
	}

	public synchronized void resume() {
		if (updateTimer != null) return;
		updateTimer = new Timer();
		updateTimer.scheduleAtFixedRate(new CalculateFusedOrientationTask(), 0, updatePeriod);

		sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
		sensorManager.registerListener(this, sensorGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
		sensorManager.registerListener(this, sensorMagnetic, SensorManager.SENSOR_DELAY_FASTEST);
	}

	public synchronized void pause() {
		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}
		sensorManager.unregisterListener(this, sensorAccelerometer);
		sensorManager.unregisterListener(this, sensorGyroscope);
		sensorManager.unregisterListener(this, sensorMagnetic);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
			case Sensor.TYPE_ACCELEROMETER:
				System.arraycopy(event.values, 0, accelValues, 0, 3);
				if (SensorManager.getRotationMatrix(rotationMatrix, null, accelValues, magnetValues)) {
					SensorManager.getOrientation(rotationMatrix, accMagOrientation);
				}
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				System.arraycopy(event.values, 0, magnetValues, 0, 3);
				break;
			case Sensor.TYPE_GYROSCOPE:
				processGyroscopeValues(event);
				break;
		}
	}

	// Performs the integration of the gyroscope data and writes the gyroscope based orientation into gyroOrientation
	private void processGyroscopeValues(SensorEvent event) {
		// don't startDynWorld until first accelerometer/magnetometer orientation has been acquired

		if (!gyroInitialized) {
			float[] initMatrix = getRotationMatrixFromOrientation(accMagOrientation);
			float[] test = new float[3];
			SensorManager.getOrientation(initMatrix, test);
			gyroMatrix = matrixMultiplication(gyroMatrix, initMatrix);
			gyroInitialized = true;
		}

		// copy the new gyro values into the gyro array
		// convert the raw gyro data into a rotation vector
		float[] deltaVector = new float[4];
		if (gyroTimestamp != 0) {
			final float dT = (event.timestamp - gyroTimestamp) * NS2S;
			System.arraycopy(event.values, 0, gyroValues, 0, 3);
			getRotationVectorFromGyro(gyroValues, deltaVector, dT / 2.0f);
		}

		// measurement done, save current time for next interval
		gyroTimestamp = event.timestamp;

		// convert rotation vector into rotation matrix
		float[] deltaMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(deltaMatrix, deltaVector);

		// apply the new rotation interval on the gyroscope based rotation matrix
		gyroMatrix = matrixMultiplication(gyroMatrix, deltaMatrix);

		// get the gyroscope based orientation from the rotation matrix
		SensorManager.getOrientation(gyroMatrix, gyroOrientation);
	}

	// Calculates a rotation vector from the gyroscope angular speed values
	private static void getRotationVectorFromGyro(float[] gyroValues, float[] deltaRotationVector, float timeFactor) {
		float[] normValues = new float[3];

		// Calculate the angular speed of the sample
		float omegaMagnitude = (float) Math.sqrt(gyroValues[0] * gyroValues[0] + gyroValues[1] * gyroValues[1] + gyroValues[2] * gyroValues[2]);

		// Normalize the rotation vector if it's big enough to get the axis
		if (omegaMagnitude > EPSILON) {
			normValues[0] = gyroValues[0] / omegaMagnitude;
			normValues[1] = gyroValues[1] / omegaMagnitude;
			normValues[2] = gyroValues[2] / omegaMagnitude;
		}

		// Integrate around this axis with the angular speed by the timestep
		// in order to get a delta rotation from this sample over the timestep
		// We will convert this axis-angle representation of the delta rotation
		// into a quaternion before turning it into the rotation matrix.
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}

	private static float[] getRotationMatrixFromOrientation(float[] o) {
		float sinX = (float) Math.sin(o[1]);
		float cosX = (float) Math.cos(o[1]);
		float sinY = (float) Math.sin(o[2]);
		float cosY = (float) Math.cos(o[2]);
		float sinZ = (float) Math.sin(o[0]);
		float cosZ = (float) Math.cos(o[0]);

		// rotation about x-axis (pitch), y-axis (roll), z-axis (azimuth)
		float[] xM = new float[]{1f, 0f, 0f, 0f, cosX, sinX, 0f, -sinX, cosX};
		float[] yM = new float[]{cosY, 0f, sinY, 0f, 1f, 0f, -sinY, 0f, cosY};
		float[] zM = new float[]{cosZ, sinZ, 0f, -sinZ, cosZ, 0f, 0f, 0f, 1f};

		// rotation order is y, x, z (roll, pitch, azimuth)
		float[] resultMatrix = matrixMultiplication(xM, yM);
		resultMatrix = matrixMultiplication(zM, resultMatrix);
		return resultMatrix;
	}

	private static float[] matrixMultiplication(float[] A, float[] B) {
		return new float[]{
				A[0] * B[0] + A[1] * B[3] + A[2] * B[6],
				A[0] * B[1] + A[1] * B[4] + A[2] * B[7],
				A[0] * B[2] + A[1] * B[5] + A[2] * B[8],
				A[3] * B[0] + A[4] * B[3] + A[5] * B[6],
				A[3] * B[1] + A[4] * B[4] + A[5] * B[7],
				A[3] * B[2] + A[4] * B[5] + A[5] * B[8],
				A[6] * B[0] + A[7] * B[3] + A[8] * B[6],
				A[6] * B[1] + A[7] * B[4] + A[8] * B[7],
				A[6] * B[2] + A[7] * B[5] + A[8] * B[8]
		};
	}

	// ===================================

	private class CalculateFusedOrientationTask extends TimerTask {
		@Override
		public void run() {
			float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;

            /*
             * Fix for 179 degrees <--> -179 degrees transition problem:
             * Check whether one of the two orientation angles (gyro or accMag) is negative while the other one is positive.
             * If so, add 360 degrees (2 * math.PI) to the negative value, perform the sensor fusion, and remove the 360 degress from the result
             * if it is greater than 180 degrees. This stabilizes the output in positive-to-negative-transition cases.
             */

			// azimuth
			if (gyroOrientation[0] < -0.5 * Math.PI && accMagOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT * (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[0]);
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI : 0;
			} else if (accMagOrientation[0] < -0.5 * Math.PI && gyroOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * (accMagOrientation[0] + 2.0 * Math.PI));
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI : 0;
			} else {
				fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * accMagOrientation[0];
			}

			// pitch
			if (gyroOrientation[1] < -0.5 * Math.PI && accMagOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT * (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[1]);
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI : 0;
			} else if (accMagOrientation[1] < -0.5 * Math.PI && gyroOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * (accMagOrientation[1] + 2.0 * Math.PI));
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI : 0;
			} else {
				fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * accMagOrientation[1];
			}

			// roll
			if (gyroOrientation[2] < -0.5 * Math.PI && accMagOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT * (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[2]);
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI : 0;
			} else if (accMagOrientation[2] < -0.5 * Math.PI && gyroOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * (accMagOrientation[2] + 2.0 * Math.PI));
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI : 0;
			} else {
				fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * accMagOrientation[2];
			}

			// overwrite gyro matrix and orientation with fused orientation to compensate gyro drift
			gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
			System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);

			if (listener != null) {
				if (handler != null) {
					handler.post(() -> listener.onDeviceRotate(fusedOrientation[0], fusedOrientation[1], fusedOrientation[2]));
				} else {
					listener.onDeviceRotate(fusedOrientation[0], fusedOrientation[1], fusedOrientation[2]);
				}
			}

		}
	}

}
