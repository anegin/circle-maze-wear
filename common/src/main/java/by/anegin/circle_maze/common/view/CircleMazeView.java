package by.anegin.circle_maze.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.dyn4j.geometry.Vector2;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import by.anegin.circle_maze.common.R;
import by.anegin.circle_maze.common.dyn.CircleMazeWorld;
import by.anegin.circle_maze.common.maze.CircleMaze;
import by.anegin.circle_maze.common.model.Clock;
import by.anegin.circle_maze.common.model.MazeSwitcher;

public class CircleMazeView extends SurfaceView implements SurfaceHolder.Callback {

	private static final double MAX_WORLD_SIZE = 10.0;  // размер поля в метрах

	private final CircleMazeWorld world;
	private final Vector2 gravity = new Vector2(0, 0);

	private Clock clock;
	private MazeSwitcher mazeSwitcher;

	private UpdateThread updateThread = null;

	private int cylinders;
	private int initialSectors;

	private int points;

	public CircleMazeView(Context context) {
		this(context, null);
	}

	public CircleMazeView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CircleMazeView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		if (!isInEditMode()) setZOrderOnTop(true);
		SurfaceHolder holder = getHolder();
		holder.setFormat(PixelFormat.TRANSPARENT);
		holder.addCallback(this);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleMazeView, defStyleAttr, 0);
		boolean clockEnabled = a.getBoolean(R.styleable.CircleMazeView_circle_maze_clock_enabled, true);
		int bgColor = a.getColor(R.styleable.CircleMazeView_circle_maze_bg_color, Color.BLACK);
		int lineColor = a.getColor(R.styleable.CircleMazeView_circle_maze_line_color, Color.BLACK);
		int ballColor = a.getColor(R.styleable.CircleMazeView_circle_maze_ball_color, Color.WHITE);
		cylinders = a.getInteger(R.styleable.CircleMazeView_circle_maze_cylinders, 2);
		initialSectors = a.getInteger(R.styleable.CircleMazeView_circle_maze_initial_sectors, 8);
		a.recycle();

		float dp32 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, context.getResources().getDisplayMetrics());

		world = new CircleMazeWorld(context, bgColor, lineColor, ballColor, MAX_WORLD_SIZE);
		world.setDrawPadding(clockEnabled ? dp32 : 0);
		world.setGravity(gravity);
		world.setTestMode(false);

		if (cylinders > 0 && initialSectors > 0) {
			CircleMaze maze = new CircleMaze(cylinders, initialSectors);
			maze.generate();
			world.setMaze(maze);
		}

		if (clockEnabled) {
			Calendar calendar = GregorianCalendar.getInstance(Locale.getDefault());
			clock = new Clock(dp32, calendar, lineColor);
			clock.setDrawMode(false, false, false);
		}

		mazeSwitcher = new MazeSwitcher(lineColor);
	}

	public void setGravity(double x, double y) {
		gravity.set(x, y);
	}

	public void setCircleMazeWorldListener(CircleMazeWorld.Listener listener) {
		world.setListener(listener);
	}

	public void setDrawMode(boolean ambient, boolean lowBit, boolean mute) {
		world.setDrawMode(ambient, lowBit, mute);
		invalidate();
	}

	public void setScore(Integer points) {
		this.points = points;
		if (updateThread != null) {
			updateThread.setScore(points);
		}
	}

	public void regenerateMaze() {
		mazeSwitcher.setListener(() -> {
			mazeSwitcher.setListener(null);

			CircleMaze maze = new CircleMaze(cylinders, initialSectors);
			maze.generate();
			world.setMaze(maze);

		});
		mazeSwitcher.animateSwicth();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		updateThread = new UpdateThread(world, clock, mazeSwitcher, holder);
		updateThread.setScore(points);
		updateThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (updateThread == null) return;
		boolean retry = true;
		updateThread.finish();
		while (retry) {
			try {
				updateThread.join();
				retry = false;
			} catch (InterruptedException ignored) {
			}
		}
		updateThread = null;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	private static class UpdateThread extends Thread {

		private final CircleMazeWorld world;
		private final Clock clock;
		private final MazeSwitcher mazeSwitcher;
		private final SurfaceHolder holder;

		private boolean running = true;

		private long lastUpdateTime = System.nanoTime();

		private long calculateTime;
		private long renderTime;
		private double fps;

		private Integer score;

		UpdateThread(CircleMazeWorld world, Clock clock, MazeSwitcher mazeSwitcher, SurfaceHolder holder) {
			this.world = world;
			this.clock = clock;
			this.mazeSwitcher = mazeSwitcher;
			this.holder = holder;
		}

		synchronized void setScore(Integer score) {
			this.score = score;
		}

		@Override
		public void run() {
			while (isRunning()) {

				long tm1 = System.nanoTime();
				if (world.getBodyCount() > 0) {
					long now = System.nanoTime();
					double elapsedTime = (now - lastUpdateTime) / 1.0e9;
					world.update(elapsedTime);
					lastUpdateTime = now;
				}

				long tm2 = System.nanoTime();
				Canvas canvas = null;
				try {
					canvas = holder.lockCanvas();
					synchronized (holder) {
						if (canvas != null) canvas.drawColor(Color.BLACK);
						world.draw(canvas, calculateTime, renderTime, fps, score);
						mazeSwitcher.draw(canvas);
						if (clock != null) clock.draw(canvas, null);
					}
				} finally {
					if (canvas != null) {
						holder.unlockCanvasAndPost(canvas);
					}
				}

				double calculateTime = (tm2 - tm1) / 1.0e6;
				double renderTime = ((System.nanoTime() - tm2) / 1.0e6);
				double fps = 1.0e9 / (System.nanoTime() - tm1);
				this.calculateTime = (long) ((this.calculateTime + calculateTime) / 2.0);
				this.renderTime = (long) ((this.renderTime + renderTime) / 2.0);
				this.fps = (this.fps + fps) / 2.0;

				try {
					Thread.sleep(1);
				} catch (InterruptedException ignored) {
				}
			}
		}

		private synchronized boolean isRunning() {
			return running;
		}

		private synchronized void finish() {
			running = false;
		}

	}

}