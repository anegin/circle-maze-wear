package by.anegin.circle_maze.common.dyn;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.TypedValue;

import org.dyn4j.collision.broadphase.DynamicAABBTree;
import org.dyn4j.collision.narrowphase.Gjk;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.ContinuousDetectionMode;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Capsule;
import org.dyn4j.geometry.Convex;
import org.dyn4j.geometry.Segment;
import org.dyn4j.geometry.Transform;
import org.dyn4j.geometry.Vector2;

import java.util.Locale;
import java.util.Random;

import by.anegin.circle_maze.common.dyn.shapes.Ball;
import by.anegin.circle_maze.common.maze.CircleMaze;
import by.anegin.circle_maze.common.utils.Utils;

public class CircleMazeWorld extends World {

	private final double maxWorldSize;

	private final Paint bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint mazePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint ballPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	private final Paint mazeBitmapPaint = new Paint();

	private static final Paint segmentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	private TextPaint textPaint;

	private final Rect textRect = new Rect();

	private CircleMaze maze;

	private Bitmap mazeBitmap;
	private Bitmap mazeBitmapGrayscale;
	private Bitmap mazeBitmapLowBit;
	private int mazeBitmapSize;
	private int mazeBitmapBodyCount;

	private final double halfWorldSize;
	private double innerRadius;
	private double outerRadius;
	private double cylinderHeight;
	private double wallWidth;

	private CircleMazeBody circleMazeBody;
	private Body ballBody;

	private boolean ambientMode;
	private boolean lowBitMode;
	private boolean muteMode;

	private boolean testMode;

	private final Vector2 gravity = new Vector2(0, 0);

	private final int bgColor;
	private final int wallColor;
	private final int ballColor;

	private final Rect drawRect = new Rect();
	private float drawPadding;

	private ColorMatrixColorFilter grayscaleFilter;

	private Listener listener;

	private boolean ballReachedCenterListenerCalled;

	public interface Listener {
		void onBallReachedCenter();
	}

	public CircleMazeWorld(Context context, int bgColor, int wallColor, int ballColor, double maxWorldSize) {
		this.maxWorldSize = maxWorldSize;
		halfWorldSize = maxWorldSize / 2.0;
		this.bgColor = bgColor;
		this.wallColor = wallColor;
		this.ballColor = ballColor;

		bgPaint.setStyle(Paint.Style.FILL);

		mazePaint.setStyle(Paint.Style.STROKE);
		mazePaint.setStrokeCap(Paint.Cap.ROUND);

		ballPaint.setStyle(Paint.Style.FILL);

		if (context != null) {
			textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
			textPaint.setColor(wallColor);
			textPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, context.getResources().getDisplayMetrics()));
		}

		getSettings().setContinuousDetectionMode(ContinuousDetectionMode.BULLETS_ONLY);

		setBroadphaseDetector(new DynamicAABBTree<>());
		setNarrowphaseDetector(new Gjk());

		setGravity(gravity);

		segmentPaint.setColor(Color.RED);
		segmentPaint.setStyle(Paint.Style.STROKE);

		ColorMatrix grayScaleColorMatrix = new ColorMatrix();
		grayScaleColorMatrix.setSaturation(0);
		grayscaleFilter = new ColorMatrixColorFilter(grayScaleColorMatrix);
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

	public synchronized void setDrawMode(boolean ambient, boolean lowBit, boolean mute) {
		this.ambientMode = ambient;
		this.lowBitMode = lowBit;
		this.muteMode = mute;
	}

	public synchronized void setDrawPadding(float drawPadding) {
		this.drawPadding = drawPadding;
	}

	public synchronized void setGravity(double ax, double ay) {
		gravity.set(ax, ay);
	}

	public synchronized void setMaze(CircleMaze maze) {
		this.maze = maze;

		if (mazeBitmap != null) {
			mazeBitmap.recycle();
			mazeBitmap = null;
		}

		// определяем внутрений и наружный радиус, толщину стен
		innerRadius = halfWorldSize / (maze.cells.length + 1.5);
		cylinderHeight = (halfWorldSize - innerRadius) / (maze.cells.length + 0.2);
		wallWidth = cylinderHeight * 0.2;
		outerRadius = halfWorldSize - 1.5f * wallWidth;

		removeAllBodies();
	}

	private void createBodies() {
		removeAllBodies();

		if (maze == null) return;

		circleMazeBody = new CircleMazeBody(maze, innerRadius, outerRadius, wallWidth);
		addBody(circleMazeBody);

		// определяем радиус мяча - чуть меньше ширины прохода во внутреннее кольцо (длина дуги одного сектора на нижнем цилиндре)
		double innerCylinderDoorSize = (innerRadius - wallWidth / 2.0) * (2 * Math.PI / maze.cells[0].length) / 2.0;
		double ballRadius = 0.7 * Math.min(cylinderHeight / 2f, innerCylinderDoorSize);

		// определяем начальное положение мяча (в случайном секторе на самом верхнем цилиндре)
		int ballCylinder = maze.cells.length - 1;
		int lastCylinderSegmentsCount = maze.cells[ballCylinder].length;
		double segmentAngle = 2f * Math.PI / lastCylinderSegmentsCount;
		int segmentNum = new Random().nextInt(lastCylinderSegmentsCount);
		double segmentCenterAngle = (segmentNum * segmentAngle) + segmentAngle / 2.0;
		double lastCylinderRadiusCenter = innerRadius + ballCylinder * cylinderHeight + cylinderHeight / 2.0;
		double ballX = lastCylinderRadiusCenter * Math.cos(segmentCenterAngle);
		double ballY = lastCylinderRadiusCenter * Math.sin(segmentCenterAngle);

		ballBody = new Ball(ballRadius, 100, 0.5).getBody();
		ballBody.translate(ballX, ballY);
		addBody(ballBody);

		ballReachedCenterListenerCalled = false;
	}

	@Override
	public boolean update(double elapsedTime) {
		updateMazeBody();
		boolean worldUpdated = super.update(elapsedTime);
		if (worldUpdated && listener != null) {
			Transform ballTransform = ballBody.getTransform();
			double ballX = ballTransform.getTranslationX();
			double ballY = ballTransform.getTranslationY();
			double dist = Math.sqrt(ballX * ballX + ballY * ballY);
			if (dist < innerRadius && !ballReachedCenterListenerCalled) {
				// мяч попал в центральную ячейку лабиринта
				listener.onBallReachedCenter();
				ballReachedCenterListenerCalled = true;
			} else if (dist > innerRadius) {
				// мяч за пределами центральной ячейки
				ballReachedCenterListenerCalled = false;
			}
		}
		return worldUpdated;
	}

	private void updateMazeBody() {
		if (maze == null || circleMazeBody == null) return;

		// определяем номер цилиндра, в котором сейчас находится мяч
		Transform transform = ballBody.getTransform();
		double ballX = transform.getTranslationX();
		double ballY = transform.getTranslationY();
		double dist = Math.sqrt(ballX * ballX + ballY * ballY) - innerRadius;
		double cylinderHeight = (outerRadius - innerRadius) / maze.cells.length;
		int cylinderNum = dist >= 0 ? (int) (dist / cylinderHeight) : -1;
		if (cylinderNum >= maze.cells.length) cylinderNum = maze.cells.length - 1;

		// определяем номер сектора, в котором сейчас находится мяч
		int sectorNum = -1;
		if (cylinderNum >= 0 && cylinderNum < maze.cells.length) {
			int sectorsCount = maze.cells[cylinderNum].length;
			double sectorAngle = 2 * Math.PI / sectorsCount;

			double angle = Math.atan2(ballY, ballX);
			if (angle < 0) angle = 2 * Math.PI + angle;

			sectorNum = (int) (angle / sectorAngle);
			sectorNum = Utils.cycle(sectorNum, sectorsCount);
		}

		boolean worldUpdateRequired = circleMazeBody.setActiveCell(cylinderNum, sectorNum);
		setUpdateRequired(worldUpdateRequired);
	}

	public synchronized void draw(Canvas canvas, long calculateTime, long renderTime, double fps, Integer score) {
		draw(canvas, null, calculateTime, renderTime, fps, score);
	}

	public synchronized void draw(Canvas canvas, Rect rect, long calculateTime, long renderTime, double fps, Integer score) {
		if (canvas == null || maze == null) return;

		if (rect != null) {
			drawRect.set(rect);
		} else {
			drawRect.set(0, 0, canvas.getWidth(), canvas.getHeight());
		}

		if (drawPadding > 0f) drawRect.inset((int) drawPadding, (int) drawPadding);

		int width = drawRect.width();
		int height = drawRect.height();
		if (width <= 0 || height <= 0) return;

		if (getBodyCount() == 0) {
			createBodies();
		}

		int mazeSize = Math.min(width, height);
		double scale = mazeSize / maxWorldSize;

		if (mazeBitmap == null || mazeSize != mazeBitmapSize || getBodyCount() != mazeBitmapBodyCount) {
			mazeBitmapSize = mazeSize;
			mazeBitmapBodyCount = getBodyCount();

			if (mazeBitmap != null) mazeBitmap.recycle();
			mazeBitmap = createMazeBitmap(mazeBitmapSize, scale, false, false);

			if (mazeBitmapGrayscale != null) mazeBitmapGrayscale.recycle();
			mazeBitmapGrayscale = createMazeBitmap(mazeBitmapSize, scale, true, false);

			if (mazeBitmapLowBit != null) mazeBitmapLowBit.recycle();
			mazeBitmapLowBit = createMazeBitmap(mazeBitmapSize, scale, true, true);
		}

		// фон
		bgPaint.setColor(ambientMode ? Color.BLACK : bgColor);
		bgPaint.setAlpha(muteMode ? 100 : 255);
		canvas.drawRect(drawRect, bgPaint);

		// битмап лабиринта
		float bitmapLeft = drawRect.left + (width - mazeBitmap.getWidth()) / 2f;
		float bitmapTop = drawRect.top + (height - mazeBitmap.getHeight()) / 2f;
		if (ambientMode) {
			if (lowBitMode) {
				if (mazeBitmapLowBit != null) {
					canvas.drawBitmap(mazeBitmapLowBit, bitmapLeft, bitmapTop, mazeBitmapPaint);
				}
			} else {
				if (mazeBitmapGrayscale != null) {
					canvas.drawBitmap(mazeBitmapGrayscale, bitmapLeft, bitmapTop, mazeBitmapPaint);
				}
			}
		} else {
			mazeBitmapPaint.setAlpha(muteMode ? 100 : 255);
			if (mazeBitmap != null) {
				canvas.drawBitmap(mazeBitmap, bitmapLeft, bitmapTop, mazeBitmapPaint);
			}
		}

		// мячик
		if (ballBody != null && ballBody.getFixtureCount() > 0) {

			if (lowBitMode) {
				ballPaint.setColor(Color.WHITE);
				ballPaint.setColorFilter(null);
			} else {
				ballPaint.setColor(ballColor);
				ballPaint.setColorFilter(ambientMode ? grayscaleFilter : null);
			}

			ballPaint.setAlpha(muteMode ? 100 : 255);

			canvas.save();
			canvas.translate(drawRect.left + width / 2f, drawRect.top + height / 2f);
			canvas.scale(1, -1);
			canvas.save();
			Transform bodyTransform = ballBody.getTransform();
			canvas.translate((float) (bodyTransform.getTranslationX() * scale), (float) (bodyTransform.getTranslationY() * scale));
			canvas.rotate((float) Math.toDegrees(bodyTransform.getRotation()));
			BodyFixture ballFixture = ballBody.getFixture(0);
			Ball ball = (Ball) ballFixture.getShape();
			drawBall(canvas, ballPaint, scale, ball);
			canvas.restore();
			canvas.restore();
		}

		if (testMode && !ambientMode) {

			// рисуем элементы dyn World
			for (Body body : getBodies()) {
				canvas.save();
				canvas.translate(drawRect.left + width / 2f, drawRect.top + height / 2f);
				canvas.scale(1, -1);
				canvas.save();
				Transform bodyTransform = body.getTransform();
				canvas.translate((float) (bodyTransform.getTranslationX() * scale), (float) (bodyTransform.getTranslationY() * scale));
				canvas.rotate((float) Math.toDegrees(bodyTransform.getRotation()));
				for (BodyFixture fixture : body.getFixtures()) {
					Convex shape = fixture.getShape();
					if (shape instanceof Segment) {
						drawSegment(canvas, (Segment) shape, scale);
					} else if (shape instanceof Capsule) {
						drawCapsule(canvas, (Capsule) shape, scale);
					}
				}
				canvas.restore();
				canvas.restore();
			}

		}

		if (textPaint != null) {
			String s = null;
			if (testMode && !ambientMode) {
				s = String.format(Locale.US, "%d/%d/%d", calculateTime, renderTime, (long) fps);
			} else if (score != null) {
				s = String.format(Locale.US, "%d", score);
			}
			if (s != null) {
				textPaint.getTextBounds(s, 0, s.length(), textRect);
				float tx = drawRect.left + (width - textRect.width()) / 2f;
				float ty = drawRect.top + (height - textRect.height()) / 2f - textPaint.ascent();
				canvas.drawText(s, tx, ty, textPaint);
			}
		}

	}

	private Bitmap createMazeBitmap(int size, double scale, boolean grayscale, boolean lowBit) {
		Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmap);
		canvas.save();
		canvas.translate(size / 2f, size / 2f);
		canvas.scale(1, -1);

		float scaledInnerRadius = (float) (innerRadius * scale);
		float scaledOuterRadius = (float) (outerRadius * scale);
		float scaledWallWidth = (float) (wallWidth * scale);

		mazePaint.setStrokeWidth(scaledWallWidth);

		if (lowBit) {
			mazePaint.setColor(Color.WHITE);
			mazePaint.setMaskFilter(null);
		} else {
			mazePaint.setColor(wallColor);
			BlurMaskFilter shadowFilter = new BlurMaskFilter(scaledWallWidth * 1.5f, BlurMaskFilter.Blur.SOLID);
			mazePaint.setMaskFilter(shadowFilter);
		}

		mazePaint.setColorFilter(grayscale ? grayscaleFilter : null);

		Path path = createCircleMazePath(maze.cells, scaledInnerRadius, scaledOuterRadius);
		canvas.drawPath(path, mazePaint);

		canvas.restore();

		return bitmap;
	}

	private static void drawSegment(Canvas canvas, Segment segment, double scale) {
		Vector2[] vertices = segment.getVertices();
		canvas.drawLine((float) (vertices[0].x * scale), (float) (vertices[0].y * scale), (float) (vertices[1].x * scale), (float) (vertices[1].y * scale), segmentPaint);
	}

	private static void drawCapsule(Canvas canvas, Capsule capsule, double scale) {
		Vector2 center = capsule.getCenter();

		canvas.save();
		canvas.translate((float) (center.x * scale), (float) (center.y * scale));
		canvas.rotate((float) Math.toDegrees(capsule.getRotation()));

		double width = capsule.getLength();
		double radius = capsule.getCapRadius();
		double radius2 = radius * 2.0;

		Path path = new Path();

		RectF arcRect = new RectF();

		double left = -(width / 2.0) * scale;
		double top = -radius * scale;
		double right = left + radius2 * scale;
		double bottom = top + radius2 * scale;
		arcRect.set((float) left, (float) top, (float) right, (float) bottom);
		path.addArc(arcRect, 90, 180);

		path.lineTo((float) ((width / 2.0 - radius) * scale), (float) top);

		left = ((width / 2.0) - radius2) * scale;
		right = left + radius2 * scale;
		arcRect.set((float) left, (float) top, (float) right, (float) bottom);
		path.addArc(arcRect, -90, 180);

		path.lineTo((float) ((-width / 2.0 + radius) * scale), (float) bottom);

		path.close();

		canvas.drawPath(path, segmentPaint);

		canvas.restore();
	}

	private static void drawBall(Canvas canvas, Paint paint, double scale, Ball ball) {
		Vector2 center = ball.getCenter();
		float cx = (float) (center.x * scale);
		float cy = (float) (center.y * scale);
		float radius = (float) (ball.getRadius() * scale);
		canvas.drawCircle(cx, cy, radius, paint);
		int color = paint.getColor();
		paint.setColor(Utils.darkerColor(color));
		canvas.drawCircle(cx + radius / 2f, cy, radius / 2.5f, paint);
		paint.setColor(color);
	}

	private static Path createCircleMazePath(int[][] cells, float innerRadius, float outerRadius) {
		float rad = innerRadius;
		float radiusStep = (outerRadius - innerRadius) / cells.length;
		Path path = new Path();
		RectF arcRect = new RectF();
		for (int i = 0; i < cells.length; i++) {
			float radiusOuter = rad + radiusStep;

			// количество секторов в цилиндре и угол сектора
			int sectorsCount = cells[i].length;
			float sectorAngle = 360f / sectorsCount;
			float halfSectorAngle = sectorAngle / 2f;

			for (int sectorNum = 0; sectorNum < sectorsCount; sectorNum++) {
				float sectorStartAngle = sectorNum * sectorAngle;
				int sectorValue = cells[i][sectorNum];
				if ((sectorValue & CircleMaze.OUT_1) == CircleMaze.OUT_1) {
					// 1-я внешняя округлая стена сектора
					arcRect.set(-radiusOuter, -radiusOuter, radiusOuter, radiusOuter);
					//canvas.drawArc(arcRect, sectorStartAngle, halfSectorAngle, false, paint);

					path.addArc(arcRect, sectorStartAngle, halfSectorAngle);
				}
				if ((sectorValue & CircleMaze.OUT_2) == CircleMaze.OUT_2) {
					// 2-я внешняя округлая стена сектора
					arcRect.set(-radiusOuter, -radiusOuter, radiusOuter, radiusOuter);
					//canvas.drawArc(arcRect, sectorStartAngle + halfSectorAngle, halfSectorAngle, false, paint);

					path.addArc(arcRect, sectorStartAngle + halfSectorAngle, halfSectorAngle);
				}
				if ((sectorValue & CircleMaze.CW) == CircleMaze.CW) {
					// передняя радиальная стена (по часовой стрелке от сектора)
					double angleRadians = Math.toRadians(sectorStartAngle + sectorAngle);
					double cos = Math.cos(angleRadians);
					double sin = Math.sin(angleRadians);
					float x1 = (float) (rad * cos);
					float y1 = (float) (rad * sin);
					float x2 = (float) (radiusOuter * cos);
					float y2 = (float) (radiusOuter * sin);
					path.moveTo(x1, y1);
					path.lineTo(x2, y2);
				}
				if (i == 0 && (sectorValue & CircleMaze.IN) == CircleMaze.IN) {
					// внутренняя стенка, только для самого нижнего цилиндра
					arcRect.set(-innerRadius, -innerRadius, innerRadius, innerRadius);
					//canvas.drawArc(arcRect, sectorStartAngle, sectorAngle, false, paint);

					path.addArc(arcRect, sectorStartAngle, sectorAngle);
				}
			}
			rad += radiusStep;
		}

		double angleStep = 2.0 * Math.PI / 60;
		double outerRadius2 = outerRadius + radiusStep * 0.075;
		double outerRadius3 = outerRadius + radiusStep * 0.15;
		for (int i = 0; i < 60; i++) {
			double angle = angleStep * i;
			double cos = Math.cos(angle);
			double sin = Math.sin(angle);
			float x1 = (float) (outerRadius * cos);
			float y1 = (float) (outerRadius * sin);
			float x2 = (float) ((i % 5 == 0 ? outerRadius3 : outerRadius2) * cos);
			float y2 = (float) ((i % 5 == 0 ? outerRadius3 : outerRadius2) * sin);
			path.moveTo(x1, y1);
			path.lineTo(x2, y2);
		}

		return path;
	}

}
