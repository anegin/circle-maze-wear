package by.anegin.circle_maze.common.dyn.shapes;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;

public class Ball extends Circle {

	private final Body body = new Body();

	public Ball(double radius, double density, double restitution) {
		super(radius);
		BodyFixture fixture = new BodyFixture(this);
		fixture.setDensity(density);
		fixture.setRestitution(restitution);
		body.addFixture(fixture);
		body.setMass(MassType.NORMAL);
		body.setAutoSleepingEnabled(false);
		body.setActive(true);
		body.setBullet(true);
	}

	public Body getBody() {
		return body;
	}

}