package by.anegin.circle_maze.common.dyn;

import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.Convex;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Segment;
import org.dyn4j.geometry.Vector2;

import java.util.ArrayList;
import java.util.List;

import by.anegin.circle_maze.common.dyn.shapes.RadialWall;
import by.anegin.circle_maze.common.maze.CircleMaze;
import by.anegin.circle_maze.common.utils.Utils;

class CircleMazeBody extends Body {

	private static final double ARC_QUALITY = 0.4; // [0; 1) чем выше тем более детальнее дуга

	private final CircleMaze maze;
	private final double innerRadius;
	private final double cylinderHeight;
	private final double wallWidth;

	private final List<Convex> wallsForCenterHole;

	private int activeCylinderNum = -1;
	private int activeSectorNum = -1;

	CircleMazeBody(CircleMaze maze, double innerRadius, double outerRadius, double wallWidth) {
		this.maze = maze;
		this.innerRadius = innerRadius;
		this.cylinderHeight = (outerRadius - innerRadius) / maze.cells.length;
		this.wallWidth = wallWidth;

		setMass(MassType.INFINITE);
		setAsleep(true);
		setBullet(false);

		wallsForCenterHole = createSolidInnerAndRadialWallsOnFirstCylinder(maze.cells[0], cylinderHeight, innerRadius, wallWidth);
	}

	boolean setActiveCell(int cylinderNum, int sectorNum) {
		if (cylinderNum >= maze.cells.length) cylinderNum = maze.cells.length - 1;

		if (activeCylinderNum != cylinderNum || activeSectorNum != sectorNum) {
			activeCylinderNum = cylinderNum;
			activeSectorNum = sectorNum;

			removeAllFixtures();

			if (cylinderNum >= 0) {

				List<Convex> walls = new ArrayList<>();
				walls.addAll(createRadialWallsForCell(maze, cylinderNum, sectorNum, innerRadius, cylinderHeight, wallWidth));
				walls.addAll(createArcWallsForCell(maze, cylinderNum, sectorNum, innerRadius, cylinderHeight, wallWidth));
				for (Convex wall : walls) addFixture(wall);

			} else if (cylinderNum < 0) {

				// in-стены и radial-стены нулевого цилиндра
				for (Convex wall : wallsForCenterHole) addFixture(wall);

			}

			return true;
		}
		return false;
	}

	// создаем радиальные (cw,ccw) стены для указанной ячейки и соседних ячеек (сверху и снизу)
	private static List<Convex> createRadialWallsForCell(CircleMaze maze, int cylinderNum, int sectorNum, double innerRadius, double cylinderHeight, double wallWidth) {
		List<Convex> walls = new ArrayList<>();

		int sectorsCount = maze.cells[cylinderNum].length;
		double sectorAngle = 2 * Math.PI / sectorsCount;

		double cylinderInnerRadius = innerRadius + cylinderNum * cylinderHeight;

		int cellValue = maze.cells[cylinderNum][sectorNum];

		// определяем значения ячеек вверху и внизу, чтобы определить у них наличие cw-стенок
		int innerCellValue = -1;
		int outerCellValue = -1;
		if (cylinderNum > 0) {
			// если есть ячейки внизу
			int inSectorsCount = maze.cells[cylinderNum - 1].length;
			if (inSectorsCount == sectorsCount) {
				innerCellValue = maze.cells[cylinderNum - 1][sectorNum];
			} else if (inSectorsCount < sectorsCount) {
				if (sectorNum % 2 == 1) {
					innerCellValue = maze.cells[cylinderNum - 1][sectorNum / 2];
				}
			}
		}
		if (cylinderNum < maze.cells.length - 1) {
			// если есть ячейки вверху
			int outSectorsCount = maze.cells[cylinderNum + 1].length;
			if (outSectorsCount == sectorsCount) {
				outerCellValue = maze.cells[cylinderNum + 1][sectorNum];
			} else if (outSectorsCount > sectorsCount) {
				outerCellValue = maze.cells[cylinderNum + 1][sectorNum * 2 + 1];
			}
		}
		// формируем непрерывные интервалы для cw-стенок (*** =** *=* **= =*= ==* *== ===)
		double cwAngle = (sectorNum + 1) * sectorAngle;
		List<double[]> intervals = Utils.createSolidIntervals(
				new boolean[]{
						innerCellValue > 0 && (innerCellValue & CircleMaze.CW) > 0,
						cellValue > 0 && (cellValue & CircleMaze.CW) > 0,
						outerCellValue > 0 && (outerCellValue & CircleMaze.CW) > 0
				},
				cylinderInnerRadius - cylinderHeight, cylinderHeight);
		for (double[] interval : intervals)
			walls.add(new RadialWall(interval[0], interval[1], cwAngle, wallWidth));

		// определяем значения ячеек вверху и внизу, чтобы определить у них наличие ccw-стенок
		innerCellValue = -1;
		outerCellValue = -1;
		if (cylinderNum > 0) {
			// если есть ячейки внизу
			int inSectorsCount = maze.cells[cylinderNum - 1].length;
			if (inSectorsCount == sectorsCount) {
				innerCellValue = maze.cells[cylinderNum - 1][sectorNum];
			} else if (inSectorsCount < sectorsCount) {
				if (sectorNum % 2 == 0) {
					innerCellValue = maze.cells[cylinderNum - 1][sectorNum / 2];
				}
			}
		}
		if (cylinderNum < maze.cells.length - 1) {
			// если есть ячейки вверху
			int outSectorsCount = maze.cells[cylinderNum + 1].length;
			if (outSectorsCount == sectorsCount) {
				outerCellValue = maze.cells[cylinderNum + 1][sectorNum];
			} else if (outSectorsCount > sectorsCount) {
				outerCellValue = maze.cells[cylinderNum + 1][sectorNum * 2];
			}
		}
		// формируем непрерывные интервалы для ccw-стенок (*** =** *=* **= =*= ==* *== ===)
		double ccwAngle = sectorNum * sectorAngle;
		intervals = Utils.createSolidIntervals(
				new boolean[]{
						innerCellValue > 0 && (innerCellValue & CircleMaze.CCW) > 0,
						cellValue > 0 && (cellValue & CircleMaze.CCW) > 0,
						outerCellValue > 0 && (outerCellValue & CircleMaze.CCW) > 0
				},
				cylinderInnerRadius - cylinderHeight, cylinderHeight);
		for (double[] interval : intervals)
			walls.add(new RadialWall(interval[0], interval[1], ccwAngle, wallWidth));

		// если вверху больше секторов, то проверяем среднюю стенку на верхнем уровне
		if (cylinderNum < maze.cells.length - 1) {
			int outSectorsCount = maze.cells[cylinderNum + 1].length;
			if (outSectorsCount > sectorsCount) {
				int out1CellValue = maze.cells[cylinderNum + 1][sectorNum * 2];
				int out2CellValue = maze.cells[cylinderNum + 1][sectorNum * 2 + 1];
				if ((out1CellValue & CircleMaze.CW) > 0 || (out2CellValue & CircleMaze.CCW) > 0) {
					double angle = ccwAngle + sectorAngle / 2.0;
					double minRadius = cylinderInnerRadius + cylinderHeight;
					double maxRadius = minRadius + cylinderHeight;
					walls.add(new RadialWall(minRadius, maxRadius, angle, wallWidth));
				}
			}
		}

		return walls;
	}

	// создаем округлые (in,out) стены для указанной ячейки и соседних ячеек (сверху и снизу)
	private static List<Convex> createArcWallsForCell(CircleMaze maze, int cylinderNum, int sectorNum, double innerRadius, double cylinderHeight, double wallWidth) {
		List<Convex> walls = new ArrayList<>();

		int sectorsCount = maze.cells[cylinderNum].length;
		double sectorAngle = 2 * Math.PI / sectorsCount;

		int ccwSectorNum = Utils.cycle(sectorNum - 1, sectorsCount);
		int cwSectorNum = Utils.cycle(sectorNum + 1, sectorsCount);

		int ccwCellValue = maze.cells[cylinderNum][ccwSectorNum];
		int cwCellValue = maze.cells[cylinderNum][cwSectorNum];
		int cellValue = maze.cells[cylinderNum][sectorNum];

		double cellAngle = sectorNum * sectorAngle;
		double cylinderInnerRadius = innerRadius + cylinderNum * cylinderHeight;
		double cylinderOuterRadius = cylinderInnerRadius + cylinderHeight;

		// out-стены текущего цилиндра (по halfSectorAngle-градусов)
		double halfSectorAngle = sectorAngle / 2.0;
		boolean[] steps = new boolean[]{
				(ccwCellValue & CircleMaze.OUT_2) > 0,
				(cellValue & CircleMaze.OUT_1) > 0,
				(cellValue & CircleMaze.OUT_2) > 0,
				(cwCellValue & CircleMaze.OUT_1) > 0
		};
		// интервалы по пол-угла текущего сектора
		List<double[]> intervals = Utils.createSolidIntervals(steps, cellAngle - halfSectorAngle, halfSectorAngle);
		for (double[] interval : intervals) {
			walls.addAll(createArcWall(interval[0], interval[1] - interval[0], cylinderOuterRadius, wallWidth));
		}

		// out-стены нижележащего цинидра
		intervals = null;
		if (cylinderNum > 0) {
			// если внизу еще есть цилиндр, смотрим сколько внизу секторов
			int inSectorsCount = maze.cells[cylinderNum - 1].length;
			if (sectorsCount == inSectorsCount) {

				// если внизу столько же секторов, то смотрим out-стены нижнего цилиндра
				//  ||   B2   ||   B1   ||   B0   || - мы на этом уровне
				//  ||====----||====----||====----||
				//  ||   A2   ||   A1   ||   A0   ||
				//      (cw)            /\  (ccw)

				ccwCellValue = maze.cells[cylinderNum - 1][ccwSectorNum]; // #A0
				cellValue = maze.cells[cylinderNum - 1][sectorNum];       // #A1
				cwCellValue = maze.cells[cylinderNum - 1][cwSectorNum];   // #A2

				steps = new boolean[]{
						(ccwCellValue & CircleMaze.OUT_2) > 0,  // в #A0 есть out_2 стена
						(cellValue & CircleMaze.OUT_1) > 0,     // в #A1 есть out_1 стена
						(cellValue & CircleMaze.OUT_2) > 0,     // в #A1 есть out_2 стена
						(cwCellValue & CircleMaze.OUT_1) > 0    // в #A2 есть out_1 стена
				};
				// интервалы по пол-угла текущего сектора
				intervals = Utils.createSolidIntervals(steps, cellAngle - halfSectorAngle, halfSectorAngle);

			} else if (sectorsCount > inSectorsCount) {

				// если внизу меньше секторов
				//  || B5 | B4 || B3 | B2 || B1 | B0 ||  - мы на этом уровне
				//  ||=====----||=====----||=====----||
				//  ||    A2   ||    A1   ||    A0   ||
				//       (cw)             /\   (ccw)

				int inSectorNum = sectorNum / 2;
				int inCwSectorNum = Utils.cycle(inSectorNum + 1, inSectorsCount);
				int inCcwSectorNum = Utils.cycle(inSectorNum - 1, inSectorsCount);

				ccwCellValue = maze.cells[cylinderNum - 1][inCcwSectorNum]; // #A0
				cellValue = maze.cells[cylinderNum - 1][inSectorNum];       // #A1
				cwCellValue = maze.cells[cylinderNum - 1][inCwSectorNum];   // #A2

				if (sectorNum % 2 == 0) {
					// если мы в out1-ячейке (#B2) по отношению к нижней ячейке (#A1)
					steps = new boolean[]{
							(ccwCellValue & CircleMaze.OUT_2) > 0,  // в #A0 есть out_2 стена
							(cellValue & CircleMaze.OUT_1) > 0,     // в #A1 есть out_1 стена
							(cellValue & CircleMaze.OUT_2) > 0      // в #A1 есть out_2 стена
					};
				} else {
					// если мы в out2-ячейке (#B3) по отношению к нижней ячейке (#A1)
					steps = new boolean[]{
							(cellValue & CircleMaze.OUT_1) > 0,     // в #A1 есть out_1 стена
							(cellValue & CircleMaze.OUT_2) > 0,     // в #A1 есть out_2 стена
							(cwCellValue & CircleMaze.OUT_1) > 0    // в #A2 есть out_1 стена
					};
				}

				// интервалы по целому углу текущего сектора (для нижестоящего уровня это будет по полугла сектора)
				intervals = Utils.createSolidIntervals(steps, cellAngle - sectorAngle, sectorAngle);

			}
		} else {
			// если мы уже на самом нижнем цилиндре, то смотрим in-стены текущего цилиндра
			steps = new boolean[]{
					(ccwCellValue & CircleMaze.IN) > 0,
					(cellValue & CircleMaze.IN) > 0,
					(cwCellValue & CircleMaze.IN) > 0
			};
			intervals = Utils.createSolidIntervals(steps, cellAngle - sectorAngle, sectorAngle);
		}
		if (intervals != null) {
			for (double[] interval : intervals) {
				walls.addAll(createArcWall(interval[0], interval[1] - interval[0], cylinderInnerRadius, wallWidth));
			}
		}

		return walls;
	}

	// создаем in-, cw-стены для первого цилиндра
	// нужно для ситуации, когда шар внутри центрального отверстия
	private static List<Convex> createSolidInnerAndRadialWallsOnFirstCylinder(int[] firstCylinder, double cylinderHeight, double innerRadius, double wallWidth) {
		List<Convex> walls = new ArrayList<>();

		// радиус внешней стены цилиндра
		double cylinderOuterRadius = innerRadius + cylinderHeight;

		// количество секторов в цилиндре и угол сектора
		int sectorsCount = firstCylinder.length;
		double sectorAngle = 2d * Math.PI / sectorsCount;

		// непрерывные внутренние округлые стенки
		List<int[]> solidInnerWalls = new ArrayList<>();
		int solidInnerWallStartSector = -1;

		for (int sectorNum = 0; sectorNum < sectorsCount; sectorNum++) {
			double sectorStartAngle = sectorNum * sectorAngle;
			int sectorValue = firstCylinder[sectorNum];

			// cw-стенка
			if ((sectorValue & CircleMaze.CW) > 0) {
				walls.add(new RadialWall(innerRadius, cylinderOuterRadius, sectorStartAngle + sectorAngle, wallWidth));
			}

			// in-стенка
			if ((sectorValue & CircleMaze.IN) > 0) {
				// обнаружено начало сплошной in-стенки
				if (solidInnerWallStartSector == -1) solidInnerWallStartSector = sectorNum;
			} else if (solidInnerWallStartSector != -1) {
				// обнаружен конец сплошной in-стенки - запоминаем ее
				solidInnerWalls.add(new int[]{solidInnerWallStartSector, sectorNum - 1});
				solidInnerWallStartSector = -1;
			}
		}

		// замыкаем последнюю сплошную in-стенку (если есть)
		if (solidInnerWallStartSector != -1) {
			solidInnerWalls.add(new int[]{solidInnerWallStartSector, sectorsCount - 1});
		}

		if (solidInnerWalls.size() > 1) {
			// проверяем на возможность объединения последнего и первого сектора для in-стен
			int[] firstSolidSector = solidInnerWalls.get(0);
			int[] lastSolidSector = solidInnerWalls.get(solidInnerWalls.size() - 1);
			if (firstSolidSector[0] == 0 && lastSolidSector[1] == sectorsCount - 1) {
				// если конец последнего сектора соприкасается с началом первого сектора, то объединяем их в один
				firstSolidSector[0] = lastSolidSector[0];
				solidInnerWalls.remove(solidInnerWalls.size() - 1);
			}
		}

		// отрисовываем непрерывные in-стенки
		for (int[] solidWall : solidInnerWalls) {
			double startAngle = solidWall[0] * sectorAngle;
			double endAngle = (solidWall[1] + 1) * sectorAngle;
			double sweepAngle;
			if (endAngle > startAngle) {
				sweepAngle = endAngle - startAngle;
			} else {
				sweepAngle = 2 * Math.PI - (startAngle - endAngle);
			}
			walls.addAll(createArcWall(startAngle, sweepAngle, innerRadius, wallWidth));
		}

		return walls;
	}

	// ==============================

	private static List<Segment> createArcWall(double startAngle, double sweepAngle, double radius, double wallWidth) {
		double halfWidth = wallWidth / 2.0;
		double outerRadius = radius + halfWidth;
		double innerRadius = radius - halfWidth;

		if (sweepAngle < 0) {
			startAngle += sweepAngle;
			sweepAngle = -sweepAngle;
		}

		int outerSegmentsCount = (int) (Math.abs(outerRadius * sweepAngle) / (1.0 - ARC_QUALITY)) + 1;
		int innerSegmentsCount = (int) (Math.abs(innerRadius * sweepAngle) / (1.0 - ARC_QUALITY)) + 1;

		List<Segment> list = new ArrayList<>();

		Vector2 prevPoint = null;
		double angleStep = 1.0 * sweepAngle / outerSegmentsCount;
		double angle = startAngle;
		for (int i = 0; i <= outerSegmentsCount; i++) {
			Vector2 nextPoint = new Vector2(outerRadius * Math.cos(angle), outerRadius * Math.sin(angle));
			if (prevPoint != null) list.add(new Segment(prevPoint, nextPoint));
			prevPoint = nextPoint;
			angle += angleStep;
		}
		angle -= angleStep;

		angleStep = 1.0 * sweepAngle / innerSegmentsCount;
		for (int i = 0; i <= innerSegmentsCount; i++) {
			Vector2 nextPoint = new Vector2(innerRadius * Math.cos(angle), innerRadius * Math.sin(angle));
			if (prevPoint != null) list.add(new Segment(prevPoint, nextPoint));
			prevPoint = nextPoint;
			angle -= angleStep;
		}

		if (prevPoint != null && list.size() > 0) {
			list.add(new Segment(prevPoint, list.get(0).getPoint1()));
		}

		return list;
	}

}