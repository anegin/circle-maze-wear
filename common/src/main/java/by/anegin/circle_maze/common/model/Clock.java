package by.anegin.circle_maze.common.model;

import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;

import java.util.Calendar;

public class Clock {

	private final TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
	private final Rect drawRect = new Rect();
	private final Rect textRect = new Rect();

	private final Paint hourPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
	private final Paint minutePaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
	private final Paint secondPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

	private boolean secondsEnabled;

	private final int digitsColor;
	private final float digitsHeight;

	private BlurMaskFilter shadowFilter;
	private ColorMatrixColorFilter grayscaleFilter;

	private final Calendar calendar;

	public Clock(float digitsHeight, Calendar calendar, int digitsColor) {
		this.digitsColor = digitsColor;
		this.digitsHeight = digitsHeight;
		this.calendar = calendar;

		textPaint.setTextSize(digitsHeight * 0.9f);
		textPaint.setFakeBoldText(true);

		shadowFilter = new BlurMaskFilter(12f, BlurMaskFilter.Blur.SOLID);

		ColorMatrix grayScaleColorMatrix = new ColorMatrix();
		grayScaleColorMatrix.setSaturation(0);
		grayscaleFilter = new ColorMatrixColorFilter(grayScaleColorMatrix);

		hourPaint.setColor(Color.WHITE);
		hourPaint.setStyle(Paint.Style.STROKE);
		hourPaint.setStrokeWidth(8f);
		hourPaint.setStrokeCap(Paint.Cap.ROUND);

		minutePaint.setColor(Color.WHITE);
		minutePaint.setStyle(Paint.Style.STROKE);
		minutePaint.setStrokeWidth(4f);
		minutePaint.setStrokeCap(Paint.Cap.ROUND);

		secondPaint.setColor(Color.RED);
		secondPaint.setStyle(Paint.Style.STROKE);
		secondPaint.setStrokeWidth(1.5f);
		secondPaint.setStrokeCap(Paint.Cap.ROUND);
		secondPaint.setMaskFilter(shadowFilter);

	}

	public synchronized void setDrawMode(boolean ambient, boolean lowBit, boolean mute) {
		if (ambient) {
			if (lowBit) {
				textPaint.setColor(Color.WHITE);
				textPaint.setMaskFilter(null);
				textPaint.setColorFilter(null);

				hourPaint.setMaskFilter(null);
				minutePaint.setMaskFilter(null);
			} else {
				textPaint.setColor(digitsColor);
				textPaint.setMaskFilter(shadowFilter);
				textPaint.setColorFilter(grayscaleFilter);

				hourPaint.setMaskFilter(shadowFilter);
				minutePaint.setMaskFilter(shadowFilter);
			}
		} else {
			textPaint.setColor(digitsColor);
			textPaint.setMaskFilter(shadowFilter);
			textPaint.setColorFilter(null);

			hourPaint.setMaskFilter(shadowFilter);
			minutePaint.setMaskFilter(shadowFilter);
		}
		textPaint.setAlpha(mute ? 100 : 255);
		hourPaint.setAlpha(mute ? 100 : 255);
		minutePaint.setAlpha(mute ? 100 : 255);
		secondsEnabled = !ambient && !mute;
	}

	public synchronized void draw(Canvas canvas, Rect bounds) {
		if (canvas == null) return;

		if (bounds != null) {
			drawRect.set(bounds);
		} else {
			drawRect.set(0, 0, canvas.getWidth(), canvas.getHeight());
		}

		int width = drawRect.width();
		int height = drawRect.height();
		if (width <= 0 || height <= 0) return;

		canvas.save();
		canvas.translate(drawRect.left + width / 2f, drawRect.top + height / 2f);

		// цифры
		float radius = 0.98f * (Math.min(width, height) / 2f - digitsHeight / 2f);
		float hourAngleStep = (float) (2 * Math.PI / 12);
		float angle = -hourAngleStep * 2;
		for (int i = 1; i <= 12; i++) {
			canvas.save();
			canvas.rotate((float) Math.toDegrees(angle));
			canvas.translate(radius, 0);
			canvas.rotate(90);

			String s = String.valueOf(i);
			textPaint.getTextBounds(s, 0, s.length(), textRect);
			canvas.drawText(s, -textRect.exactCenterX(), -textRect.exactCenterY(), textPaint);

			canvas.restore();

			angle += hourAngleStep;
		}

		calendar.setTimeInMillis(System.currentTimeMillis());
		int hour = calendar.get(Calendar.HOUR);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int milliseconds = calendar.get(Calendar.MILLISECOND);

		// часовая стрелка
		angle = hourAngleStep * (hour - 3f) + minute * (hourAngleStep / 60f);
		canvas.save();
		canvas.rotate((float) Math.toDegrees(angle));
		canvas.drawLine(0, 0, 0.7f * radius, 0, hourPaint);
		canvas.restore();

		// минутная стрелка
		float minuteAngleStep = (float) (2 * Math.PI / 60);
		angle = minuteAngleStep * (minute - 15f) + second * (minuteAngleStep / 60f);
		canvas.save();
		canvas.rotate((float) Math.toDegrees(angle));
		canvas.drawLine(0, 0, 0.9f * radius, 0, minutePaint);
		canvas.restore();

		// секундная стрелка
		if (secondsEnabled) {
			float secondAngleStep = (float) (2 * Math.PI / 60);
			angle = secondAngleStep * (second - 15f) + milliseconds * (secondAngleStep / 1000f);
			canvas.save();
			canvas.rotate((float) Math.toDegrees(angle));
			canvas.drawLine(0, 0, 0.9f * radius, 0, secondPaint);
			canvas.restore();
		}

		canvas.restore();
	}
}
