package by.anegin.circle_maze.common.dyn.shapes;

import org.dyn4j.geometry.Capsule;

public class RadialWall extends Capsule {

	public RadialWall(double startRadius, double endRadius, double angle, double width) {
		super(width + (endRadius - startRadius), width);
		translate(((endRadius - startRadius)) / 2.0 + startRadius, 0);
		rotate(angle);
	}

}