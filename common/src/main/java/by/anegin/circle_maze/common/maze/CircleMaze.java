package by.anegin.circle_maze.common.maze;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import by.anegin.circle_maze.common.utils.Utils;

public class CircleMaze {

	public static final int CW = 1;            // по часовой стрелке
	public static final int CCW = 2;           // против часовой стрелки
	public static final int IN = 4;            // в сторону центра (внутрь)
	public static final int OUT_1 = 8;         // в сторону от центра (наружу)
	public static final int OUT_2 = 16;        // в сторону от центра (наружу)

	private static final int ALL = CW | CCW | IN | OUT_1 | OUT_2;

	private final Random rnd = new Random();

	private final int cylinders;    // кол-во цилиндров

	public final int[][] cells;

	public CircleMaze(int cylinders, int initialSectors) {
		this.cylinders = cylinders;
		this.cells = new int[cylinders][];
		int sectorsCount = initialSectors;
		int nextStep = 2;
		int next = 2;
		for (int cyl = 0; cyl < cylinders; cyl++) {
			cells[cyl] = new int[sectorsCount];
			if (cyl + 1 == next) {
				sectorsCount *= 2;
				nextStep *= 2;
				next += nextStep;
			}
		}
		resetWalls();
	}

	private void resetWalls() {
		for (int cyl = 0; cyl < cylinders; cyl++)
			for (int sect = 0; sect < cells[cyl].length; sect++) cells[cyl][sect] = ALL;
	}

	public void removeWall(int cylinder, int sector, int wall) {
		if (cylinder < 0 || cylinder > cells.length) return;

		int sectorsCount = cells[cylinder].length;
		sector = Utils.cycle(sector, sectorsCount);

		// если это последний цилиндр, и нужно удалить out-стену, то не даем этого сделать
		if (cylinder == cells.length - 1 && ((wall & CircleMaze.OUT_1) > 0 || (wall & CircleMaze.OUT_2) > 0))
			return;

		if ((wall & CircleMaze.OUT_1) > 0 && sectorsCount == cells[cylinder + 1].length) {
			// если удаляется out1 стена, и на верхнем цилиндре столько же секторов, то будет удалена также out2 стена
			wall |= CircleMaze.OUT_2;
		}

		// удаляем стену
		cells[cylinder][sector] &= ~wall;

		if ((wall & CircleMaze.CW) > 0) {
			// если удаляется cw-стена, то удаляем ccw-стену для следующей (cw) ячейки
			int cwSector = Utils.cycle(sector + 1, sectorsCount);
			cells[cylinder][cwSector] &= ~CircleMaze.CCW;
		}
		if ((wall & CircleMaze.CCW) > 0) {
			// если удаляется ccw-стена, то удаляем cw-стену для предыдущей (ccw) ячейки
			int ccwSector = Utils.cycle(sector - 1, sectorsCount);
			cells[cylinder][ccwSector] &= ~CircleMaze.CW;
		}
		if (cylinder > 0 && (wall & CircleMaze.IN) > 0) {
			// если удаляется in-стена, и внизу есть ячейки, то определяем сколько секторов на нижнем цилиндре
			int inSectorsCount = cells[cylinder - 1].length;
			if (inSectorsCount == sectorsCount) {
				// если на нижнем цилиндре столько же ячеек, то удаяем обе out-стены для нижней ячейки
				cells[cylinder - 1][sector] &= ~CircleMaze.OUT_1;
				cells[cylinder - 1][sector] &= ~CircleMaze.OUT_2;
			} else {
				// если на нижнем цилиндре меньше ячеек, то удаляем только одну out-стену
				cells[cylinder - 1][sector / 2] &= ~(sector % 2 == 0 ? CircleMaze.OUT_1 : CircleMaze.OUT_2);
			}
		}
		if (cylinder < cells.length - 1) {
			// если вверху есть ячейки, то определяем сколько секторов на верхнем цилиндре
			int outSectorsCount = cells[cylinder + 1].length;
			if (outSectorsCount == sectorsCount) {
				// если на верхнем цилиндре столько же секторов, то удаяем in-стену для верхней ячейки
				cells[cylinder + 1][sector] &= ~CircleMaze.IN;
			} else {
				// если на верхнем цилиндре больше ячеек, то удаляем только одну out-стену
				int outSector1 = sector * 2;
				if ((wall & CircleMaze.OUT_1) > 0) {
					cells[cylinder + 1][outSector1] &= ~CircleMaze.IN;
				}
				if ((wall & CircleMaze.OUT_2) > 0) {
					cells[cylinder + 1][outSector1 + 1] &= ~CircleMaze.IN;
				}
			}
		}
	}

	public void generate() {
		resetWalls();

		// начинаем с рандомной ячейки
		int cylinder = rnd.nextInt(cylinders);
		int sector = rnd.nextInt(cells[cylinder].length);
		loop(cylinder, sector, 0);

		// добавляем проход в центральный сектор
		sector = rnd.nextInt(cells[0].length);
		cells[0][sector] &= ~IN;
	}

	private void loop(int cylinder, int sector, int fromDirection) {

		// доступные для движения направления
		int availableDirs = ALL;

		// запрещаем движение в направлении, из которого пришли
		availableDirs &= ~fromDirection;

		// количество секторов в текущем цилиндре
		int sectorsInCylinder = cells[cylinder].length;

		// номер сектора следующей соседней ячейки (по часовой стрелке)
		int cwSector = sector + 1;
		if (cwSector >= sectorsInCylinder) cwSector = 0;

		// номер сектора предыдущей соседней ячейки (против часовой стрелке)
		int ccwSector = sector - 1;
		if (ccwSector < 0) ccwSector = sectorsInCylinder - 1;

		// номер сектора соседней ячейки на нижестоящем цилиндре (-1 если мы уже на самом внутреннем цилиндре)
		int inSector = -1;
		if (cylinder > 0) {
			// если мы еще не на самом нижнем цилиндре
			int sectorAtInnerCylinder = cells[cylinder - 1].length;
			if (sectorAtInnerCylinder == sectorsInCylinder) {
				// если на нижестоящем цилиндре столько же секторов, то номер сектора равен текущему
				inSector = sector;
			} else if (sectorAtInnerCylinder < sectorsInCylinder) {
				// если на нижестоящем цилиндре секторов меньше, то делим текущий номер сектора на два
				inSector = sector / 2;
			}
		}

		// номер(а) сектора(ов) соседней(их) ячейки(ек) на вышестоящем цилиндре (-1 если мы уже на самом внешнем цилиндре)
		int outSector1 = -1;
		int outSector2 = -1;
		if (cylinder + 1 < cylinders) {
			// если мы еще не на самом верхнем цилиндре
			int sectorsAtOuterCylinder = cells[cylinder + 1].length;
			if (sectorsAtOuterCylinder == sectorsInCylinder) {
				// если на вышестоящем цилиндре столько же секторов, то номер сектора равен текущему
				// и вверху только один сектор
				outSector1 = sector;
			} else if (sectorsAtOuterCylinder > sectorsInCylinder) {
				// если на вышестоящем цилиндре секторов больше, то умножаем текущий номер сектора на два
				// и вверху будет два сектора
				outSector1 = sector * 2;
				outSector2 = outSector1 + 1;
			}
		}

		// переходим в случайном направлении, пока есть доступные направления
		int dirsCount;
		do {

			// проверяем оставшиеся доступные направления

			if (cells[cylinder][cwSector] != ALL) {
				// если следующая соседняя ячейка (по часовой стрелке) уже была посещена
				// то запрещаем движение по часовой стрелке
				availableDirs &= ~CW;
			}

			if (cells[cylinder][ccwSector] != ALL) {
				// если предыдущая соседняя ячейка (против часовой стрелки) уже была посещена
				// то запрещаем движение против часовой стрелки
				availableDirs &= ~CCW;
			}

			if (cylinder - 1 < 0 || cells[cylinder - 1][inSector] != ALL) {
				// если мы достигли самого внутреннего цилиндра ИЛИ соседняя ячейка ниже уже была посещена
				// то запрещаем движение к центру (внутрь)
				availableDirs &= ~IN;
			}

			if (cylinder + 1 >= cylinders) {
				// если мы достигли самого внешнего цилиндра, то запрещаем движение от центра (наружу)
				availableDirs &= ~OUT_1;
				availableDirs &= ~OUT_2;
			} else {
				if (cells[cylinder + 1][outSector1] != ALL) {
					// если вверху нет 1-й ячейки ИЛИ 1-я ячейка вверху уже была посещена
					// то запрещаем движение от центра (наружу) в 1-ю ячейку вверху
					availableDirs &= ~OUT_1;
				}
				if (outSector2 == -1 || cells[cylinder + 1][outSector2] != ALL) {
					// если вверху нет 2-й ячейки ИЛИ 2-я ячейка вверху уже была посещена
					// то запрещаем движение от центра (наружу) во 2-ю ячейку вверху
					availableDirs &= ~OUT_2;
				}
			}

			// считаем количество доступных направлений
			dirsCount = (availableDirs & CW) == CW ? 1 : 0;
			dirsCount += (availableDirs & CCW) == CCW ? 1 : 0;
			dirsCount += (availableDirs & IN) == IN ? 1 : 0;
			dirsCount += (availableDirs & OUT_1) == OUT_1 ? 1 : 0;
			dirsCount += (availableDirs & OUT_2) == OUT_2 ? 1 : 0;

			if (dirsCount > 0) {

				// выбираем рандомно "номер" направления (от 1 до dirsCount)
				int n = rnd.nextInt(dirsCount) + 1;

				// находим выбранное направление из битовой маски, пропуская нулевые биты
				int dir = 0;
				int dirBitCounter = 0;
				for (int bitIndex = 0; bitIndex < 32; bitIndex++) {
					int bitValue = (availableDirs >> bitIndex) & 1;
					if (bitValue > 0) {
						dirBitCounter++;
						if (dirBitCounter == n) {
							// нашли N-ый по порядку ненулевой бит, получаем направление
							dir = 1 << bitIndex;
							break;
						}
					}
				}

				switch (dir) {
					case CW:

						// убираем стенку от текущей ячейки к следующей (по часовой стрелке)
						// и стенку от следующей ячейки к текущей
						cells[cylinder][sector] &= ~CW;
						cells[cylinder][cwSector] &= ~CCW;

						// двигаемся по часовой стрелке
						loop(cylinder, cwSector, CCW);
						break;

					case CCW:

						// убираем стенку от текущей ячейки к предыдущей (против часовой стрелки)
						// и стенку от предыдущей ячейки к текущей
						cells[cylinder][sector] &= ~CCW;
						cells[cylinder][ccwSector] &= ~CW;

						// двигаемся против часовой стрелки
						loop(cylinder, ccwSector, CW);
						break;

					case IN:

						// убираем нижнюю стенку для текущей ячейки
						cells[cylinder][sector] &= ~IN;

						// нужно определить, какой является текущая ячейка для нижней ячейки (1-й или 2-й)
						int outNum = OUT_1;
						int sectorAtInnerCylinder = cells[cylinder - 1].length;
						if (sectorAtInnerCylinder < sectorsInCylinder) {
							// если на нижнем уровне меньше секторов, то определяем outNum остатком от деления на 2
							if (sector % 2 == 1) outNum = OUT_2;
						}

						// убираем верхнюю стену в нижней ячейке
						cells[cylinder - 1][inSector] &= ~outNum;

						// двигаемся к центру (внутрь)
						loop(cylinder - 1, inSector, outNum);
						break;

					case OUT_1:

						// убираем 1-ю верхнюю стенку для текущей ячейки
						// и нижнюю стенку для 1-й ячейки вверху
						cells[cylinder][sector] &= ~OUT_1;
						cells[cylinder + 1][outSector1] &= ~IN;

						// двигаемся вверх в 1-ю ячейку
						loop(cylinder + 1, outSector1, IN);
						break;

					case OUT_2:

						// убираем 2-ю верхнюю стенку для текущей ячейки
						// и нижнюю стенку для 2-й ячейки вверху
						cells[cylinder][sector] &= ~OUT_2;
						cells[cylinder + 1][outSector2] &= ~IN;

						// двигаемся вверх во 2-ю ячейку
						loop(cylinder + 1, outSector2, IN);
						break;
				}

			}

		} while (dirsCount > 0);

	}

	public static String cellString(int value) {
		if (value <= 0) return "";
		List<String> list = new ArrayList<>();
		if ((value & CW) > 0) list.add("CW");
		if ((value & CCW) > 0) list.add("CCW");
		if ((value & IN) > 0) list.add("IN");
		if ((value & OUT_1) > 0) list.add("OUT_1");
		if ((value & OUT_2) > 0) list.add("OUT_2");
		return TextUtils.join(" ", list);
	}

}