package by.anegin.circle_maze.common.model;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextPaint;
import android.view.animation.LinearInterpolator;

public class MazeSwitcher {

	private static final int ANIM_DURATION = 1000;

	private ValueAnimator valueAnimator;
	private boolean expanded;

	private final Paint paint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

	public interface Listener {
		void onMazeSwitcherExpanded();
	}

	private Listener listener;

	public MazeSwitcher(int color) {
		paint.setColor(color);
		paint.setStyle(Paint.Style.FILL);
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public void animateSwicth() {
		if (valueAnimator != null && valueAnimator.isRunning()) return;

		expanded = false;

		valueAnimator = ValueAnimator.ofFloat(0f, 1f);
		valueAnimator.setInterpolator(new LinearInterpolator());
		valueAnimator.setDuration(ANIM_DURATION);
		valueAnimator.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);
				expanded = false;
				valueAnimator = null;
			}
		});
		valueAnimator.start();
	}

	public void draw(Canvas canvas) {
		if (canvas == null || valueAnimator == null) return;

		int width = canvas.getWidth();
		int height = canvas.getHeight();
		if (width <= 0 || height <= 0) return;

		float animValue = valueAnimator.getAnimatedFraction();
		float alpha = animValue <= 0.5f ? (2f * animValue) : (1f - 2f * (animValue - 0.5f));

		paint.setAlpha((int) (255 * alpha));
		canvas.drawRect(0, 0, width, height, paint);

		if (!expanded && animValue >= 0.5f) {
			expanded = true;
			if (listener != null) listener.onMazeSwitcherExpanded();
		}
	}

}