package by.anegin.circle_maze.common.utils;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

public class Utils {

	public static int darkerColor(int color) {
		float[] hsvColor = new float[3];
		Color.colorToHSV(color, hsvColor);
		hsvColor[2] *= 0.8f;
		return Color.HSVToColor(hsvColor);
	}

	public static int cycle(int value, int count) {
		return (value = value % count) < 0 ? count + value : value;
	}

	public static List<double[]> createSolidIntervals(boolean[] steps, double startValue, double valueStep) {
		List<double[]> intervals = new ArrayList<>();
		Double minValue = null;
		Double maxValue = null;
		for (int i = 0; i < steps.length; i++) {
			if (steps[i]) {
				if (minValue == null) {
					minValue = startValue + (i * valueStep);
				}
				maxValue = startValue + ((i + 1) * valueStep);
			} else {
				if (minValue != null) {
					intervals.add(new double[]{minValue, maxValue});
				}
				minValue = null;
				maxValue = null;
			}
		}
		if (minValue != null) {
			intervals.add(new double[]{minValue, maxValue});
		}
		return intervals;
	}

}
