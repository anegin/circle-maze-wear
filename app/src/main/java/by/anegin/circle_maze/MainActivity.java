package by.anegin.circle_maze;

import android.app.Activity;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import by.anegin.circle_maze.common.dyn.CircleMazeWorld;
import by.anegin.circle_maze.common.utils.SensorHelper;
import by.anegin.circle_maze.common.view.CircleMazeView;

public class MainActivity extends Activity implements SensorHelper.Listener, CircleMazeWorld.Listener {

	private static final double C = (5d / 7d) * SensorManager.GRAVITY_EARTH;

	private CircleMazeView circleMazeView;

	private SensorHelper sensorHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		circleMazeView = findViewById(R.id.circle_maze_view);
		circleMazeView.setCircleMazeWorldListener(this);

		sensorHelper = new SensorHelper(this, 30, new Handler(), this);
	}

	@Override
	public void onResume() {
		super.onResume();
		sensorHelper.resume();
	}

	@Override
	protected void onPause() {
		sensorHelper.pause();
		super.onPause();
	}

	@Override
	public void onDeviceRotate(double azimuth, double pitch, double roll) {
		double coef = 2.0;
		double ax = C * Math.sin(roll) * coef;
		double ay = C * Math.sin(pitch) * coef;
		circleMazeView.setGravity(ax, ay);
	}

	@Override
	public void onBallReachedCenter() {
		// вызывается не в ui-потоке
		runOnUiThread(() -> {
			Toast.makeText(this, "Win!!!", Toast.LENGTH_SHORT).show();
			circleMazeView.regenerateMaze();
		});
	}

}